include("shared.lua")

ENT.RenderGroup = RENDERGROUP_BOTH

function ENT:Initialize()
	self:UpdateOverlayText()
	urp.ents_list[self:EntIndex()] = self
end

function ENT:UpdateOverlayText()
	self.OverlayText = "Деньги: <color=141,161,88>" .. urp.format_money(self:GetValue()) .. "</color>"
end
