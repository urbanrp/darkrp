AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")

include("shared.lua")


local valueModels = {
	[0] = "models/props/cs_assault/Dollar.mdl",
	[100] = "models/props/cs_assault/Money.mdl",
	[5000] = "models/props_c17/BriefCase001a.mdl",
	[100000] = "models/props/cs_assault/MoneyPallet03E.mdl",
	[1000000] = "models/props/cs_assault/MoneyPallet.mdl",
}

function ENT:Initialize()
	self:SetUseType(SIMPLE_USE)
	urp.ents_list[self:EntIndex()] = self
end

function ENT:SetAmount(num, pos, ang)
	local model = valueModels[0]

	for k, v in pairs(valueModels) do
		if k <= num then
			model = v
		end
	end

	self:SetModel(model)
	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)
	self:SetValue(num)

	local phys = self:GetPhysicsObject()
	if IsValid(phys) then
		self:GetPhysicsObject():Wake()
	end
	if pos then self:SetPos(pos) end
	if ang then self:SetAngles(ang) end
end

function ENT:Use(activator, caller)
	if self.used then return end

	self.used = true
	local value = self:GetValue()

	activator:AddMoney(value or 0)
	urp.notify(activator, 0, 4, "Вы подобрали "..urp.format_money(value))
	self:Remove()
end

function ENT:OnTakeDamage(dmg)
	self:TakePhysicsDamage(dmg)

	local typ = dmg:GetDamageType()
	if bit.band(typ, DMG_BULLET) ~= DMG_BULLET then return end

	self.used = true
	self:Remove()
end

function ENT:StartTouch(ent)
	if ent:GetClass() ~= "ent_money" or self.used or ent.used then return end

	ent.used = true

	local total_money = self:GetValue() + ent:GetValue()
	self:SetAmount(total_money, ent:GetPos(), ent:GetAngles())
	ent:Remove()

	if urp.config.money_remove_timer ~= 0 then
		timer.Adjust("urp.money.remove" .. self:EntIndex(), urp.config.money_remove_timer, 1, function()
			SafeRemoveEntity(self)
		end)
	end
end
