GM.Name = "DarkRP"
GM.Author = "roni_sl, Riley"
GM.Email = ""
GM.Website = "http://flame-servers.ru"

DeriveGamemode("sandbox")

urp = urp or {}
urp.config = urp.config or {}
urp.util = urp.util or {}
urp.ents_list = urp.ents_list or {}

local include_sv = SERVER and include or function() end
local include_cl = SERVER and AddCSLuaFile or include
local include_sh = function(f)
	include_sv(f)
	include_cl(f)
end

local gamemode_folder = GM.FolderName .. "/gamemode"

-- include libs
include_sv(gamemode_folder .. "/lib/mysqlite.lua")

-- include configs
include_sh(gamemode_folder .. "/sh_config.lua")
include_sv(gamemode_folder .. "/sv_config.lua")

local core_folder = gamemode_folder .. "/core"
local modules_folder = gamemode_folder .. "/modules"

function urp.load_core()
	urp.print_debug(CYAN, "-> Loading core")

	for _, File in SortedPairs(file.Find(core_folder .. "/sh_*.lua", "LUA"), true) do
		include_sh(core_folder .. "/" .. File)
		urp.print_debug(RED, "\tcore -> ", WHITE, File)
	end

	for _, File in SortedPairs(file.Find(core_folder .. "/sv_*.lua", "LUA"), true) do
		include_sv(core_folder .. "/" .. File)
		urp.print_debug(RED, "\tcore -> ", WHITE, File)
	end

	for _, File in SortedPairs(file.Find(core_folder .. "/cl_*.lua", "LUA"), true) do
		include_cl(core_folder .. "/" .. File)
		urp.print_debug(RED, "\tcore -> ", WHITE, File)
	end

	for _, File in SortedPairs(file.Find(core_folder .. "/vgui/*.lua", "LUA"), true) do
		include_cl(core_folder .. "/vgui/" .. File)
	end
end

function urp.load_modules()
	urp.print_debug(CYAN, "-> Loading modules")
	local _, folders = file.Find(modules_folder .. "/*", "LUA")
	for _, folder in SortedPairs(folders, true) do
		if folder == "." or folder == ".." or folder == "vgui" then continue end

		local module_folder = modules_folder .. "/" .. folder

		for _, File in SortedPairs(file.Find(module_folder .. "/sh_*.lua", "LUA"), true) do
			include_sh(module_folder .. "/" .. File)
		end

		for _, File in SortedPairs(file.Find(module_folder .. "/sv_*.lua", "LUA"), true) do
			include_sv(module_folder .. "/" .. File)
		end

		for _, File in SortedPairs(file.Find(module_folder .. "/cl_*.lua", "LUA"), true) do
			include_cl(module_folder .. "/" .. File)
		end

		urp.print_debug(YELLOW, "\tmodule -> ", WHITE, folder)
	end
end

function urp.print_debug(...)
	if not urp.config.debug then return end
	MsgC(...)
	MsgN()
end

print()
urp.load_core()
urp.load_modules()
print()

function GM:Initialize()
	print("Gamemode is initialized!")
end
