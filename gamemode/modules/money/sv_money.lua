local meta = FindMetaTable("Player")

function meta:SetMoney(num)
	self:SetNWInt("urp.Money", num)
	urp.money.sync(self)
end

function meta:AddMoney(num)
	self:SetMoney(self:GetMoney() + num)
end

function meta:SetBankMoney(num)
	self:SetNWInt("urp.BankMoney", num)
end

function meta:AddBankMoney(num)
	self:SetBankMoney(self:GetBankMoney() + num)
	urp.money.sync(self)
end

function urp.money.sync(ply)
	MySQLite.query(("UPDATE urp_characters SET money_held = %d, money_bank = %d WHERE id = %d;"):format(
		ply:GetMoney(),
		ply:GetBankMoney(),
		ply.char_id or 0
	), nil, function(e) urp.print_debug("MYSQL", e) end)
end

function urp.money.create_money_ent(pos, num)
	local ent_m = ents.Create("ent_money")
	ent_m:SetAmount(math.Min(num, 2147483647))
	ent_m:SetPos(pos)
	ent_m:Spawn()
	ent_m:Activate()

	if urp.config.money_remove_timer ~= 0 then
		timer.Create("urp.money.remove" .. ent_m:EntIndex(), urp.config.money_remove_timer, 1, function()
			SafeRemoveEntity(ent_m)
		end)
	end

	return ent_m
end

hook.Add("Initialize", "urp.money.Initialize", function()

	urp.commands.register("dropmoney", USER_ACCESS, function(ply, args)
		local money = tonumber(args[1])
		if not money then
			urp.notify(ply, 1, 4, "Вы забыли указать кол-во денег")
			return ""
		end

		if money < 1 then
			urp.notify(ply, 1, 4, "Нельзя передать < 1")
			return ""
		end

		if not ply:CanAfford(money) then
			urp.notify(ply, 1, 4, "У вас не хватает денег!")
			return ""
		end

		local pos = util.TraceLine{
			start = ply:GetShootPos(),
			endpos = ply:GetShootPos() + ply:GetAimVector() * 150,
			filter = ply,
		}.HitPos

		ply:AddMoney(-money)
		urp.money.create_money_ent(pos, money)
		return ""
	end, {"dm"})

	urp.commands.register("givemoney", USER_ACCESS, function(ply, args)
		local money = tonumber(args[1])
		if not money then
			urp.notify(ply, 1, 4, "Вы забыли указать кол-во денег")
			return ""
		end

		if money < 1 then
			urp.notify(ply, 1, 4, "Нельзя выкинуть < 1")
			return ""
		end

		if not ply:CanAfford(money) then
			urp.notify(ply, 1, 4, "У вас не хватает денег!")
			return ""
		end

		local target = ply:GetEyeTrace().Entity
		if not target or not target:IsPlayer() then
			urp.notify(ply, 1, 4, "Вы должны смотреть на игрока!")
			return ""
		end

		ply:AddMoney(-money)
		target:AddMoney(money)
		return ""
	end)

end)
