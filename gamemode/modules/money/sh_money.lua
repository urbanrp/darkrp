urp.money = urp.money or {}

local meta = FindMetaTable("Player")

function meta:GetMoney()
	return self:GetNWInt("urp.Money")
end

function meta:GetBankMoney()
	return self:GetNWInt("urp.BankMoney")
end

function meta:CanAfford(num)
	return self:GetMoney() >= num
end

function urp.format_money(num)
	return urp.config.money_sign .. string.Comma(num)
end
