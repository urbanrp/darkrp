urp.chars = urp.chars or {}

local meta = FindMetaTable("Player")

function meta:GetPlayerModel()
	return self:GetNWString("urp.PlayerModel")
end

function meta:GetFirstName()
	return self:GetNWString("urp.FirstName")
end

function meta:GetLastName()
	return self:GetNWString("urp.LastName")
end

function meta:GetFullName()
	return ("%s %s"):format(self:GetFirstName(), self:GetLastName())
end
