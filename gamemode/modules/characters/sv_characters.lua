hook.Add("PlayerInitialSpawn", "urp.chars.PlayerInitialSpawn", function(ply)
	ply:SetPos(Vector(0,0,0))
	ply:Lock()
	urp.chars.get_characters(ply:SteamID(), function(chars)
		urp.chars.open_menu(ply, chars)
	end)
end)

function urp.chars.get_characters(steamid, func)
	MySQLite.query(("SELECT * FROM urp_characters WHERE steamid = %s;"):format(MySQLite.SQLStr(steamid)), function(data)
		func(data)
	end, function(e) urp.print_debug("MYSQL", e) end)
end

util.AddNetworkString("urp.chars.open_menu")
function urp.chars.open_menu(ply, chars)
	net.Start("urp.chars.open_menu")
	net.WriteTable(chars and chars or {})
	net.Send(ply)
end

function urp.chars.is_name_taken(first_name, last_name, func)
	MySQLite.query(("SELECT * FROM urp_characters WHERE first_name = %s AND last_name = %s LIMIT 1;"):format(
		MySQLite.SQLStr(first_name),
		MySQLite.SQLStr(last_name)
	), function(data)
		func(istable(data))
	end, function(e) urp.print_debug("MYSQL", e) end)
end

util.AddNetworkString("urp.chars.load_character")
util.AddNetworkString("urp.chars.load_character_status")
function urp.chars.load_character(ply, index)
	MySQLite.query(("SELECT * FROM urp_characters WHERE id = %d;"):format(index), function(data)
		if data then
			data = data[1]
			ply.char_id = tonumber(data.id)
			ply:SetMoney(tonumber(data.money_held))
			ply:SetBankMoney(tonumber(data.money_bank))
			-- ply:SetInventory(util.JSONToTable(data.inventory))
			-- ply:SetVehicles(util.JSONToTable(data.vehicles))
			-- ply:SetProperties(util.JSONToTable(data.properties))
			-- ply:SetInventoryBank(util.JSONToTable(data.inventory_bank))
			-- ply:SetNeeds(util.JSONToTable(data.needs))
			-- ply:SetSkills(util.JSONToTable(data.skills))

			ply:SetPlayerModel(data.model)
			ply:SetModel(data.model)
			ply:SetSkin(tonumber(data.model_skin))
			ply:SetBodyGroups(data.bgroups)
			ply:SetFirstName(data.first_name)
			ply:SetLastName(data.last_name)

			local pos = hook.Call("PlayerSelectSpawn", GAMEMODE, ply):GetPos()
			ply:SetPos(pos)
			ply:UnLock()
		end
		net.Start("urp.chars.load_character_status")
			net.WriteBool(istable(data))
		net.Send(ply)

		hook.Run("urp.chars.is_loaded", ply, data.id)
	end, function(e) urp.print_debug("MYSQL", e) end)
end
net.Receive("urp.chars.load_character", function(len,ply)
	if ply.char_id then return end
	local id = net.ReadInt(32)
	urp.chars.load_character(ply, id)
end)

util.AddNetworkString("urp.chars.name_is_taken")
util.AddNetworkString("urp.chars.char_create_status")
function urp.chars.create_character(ply, first_name, last_name, model, model_skin)
	urp.chars.is_name_taken(first_name, last_name, function(is_taken)
		if is_taken then
			net.Start("urp.chars.name_is_taken")
			net.Send(ply)
			return
		end

		MySQLite.query(([[
			INSERT INTO urp_characters(
				steamid,
				steamid64,
				first_name,
				last_name,
				inventory,
				money_held,
				money_bank,
				model,
				model_skin,
				model_bgroups,
				vehicles,
				properties,
				inventory_bank,
				needs,
				skills
			) VALUES (
				%s, %s, %s, %s, %s, %d, %d, %s, %d, %s, %s, %s, %s, %s, %s
			);
		]]):format(
			MySQLite.SQLStr(ply:SteamID()),
			MySQLite.SQLStr(ply:SteamID64()),
			MySQLite.SQLStr(first_name),
			MySQLite.SQLStr(last_name),
			MySQLite.SQLStr("[]"),
			urp.config.default_money,
			urp.config.default_bank_money,
			MySQLite.SQLStr(model),
			model_skin,
			MySQLite.SQLStr("[]"),
			MySQLite.SQLStr("[]"),
			MySQLite.SQLStr("[]"),
			MySQLite.SQLStr("[]"),
			MySQLite.SQLStr("[]"),
			MySQLite.SQLStr("[]")
		), function(data, index)
			local LASTINSERTID = MySQLite.isMySQL() and "LAST_INSERT_ID()" or "last_insert_rowid()"

			MySQLite.query("SELECT " .. LASTINSERTID .. " AS last_index FROM urp_characters LIMIT 1;", function(data)
				net.Start("urp.chars.char_create_status")
				net.WriteBool(true)
				net.WriteTable({id = data[1].last_index, first_name = first_name, last_name = last_name, model = model})
				net.Send(ply)
			end, function(e) urp.print_debug("MYSQL", e) end)
		end, function(e) urp.print_debug("MYSQL", e) end)
	end)
end

util.AddNetworkString("urp.chars.create_char")
net.Receive("urp.chars.create_char", function(len,ply)
	local first_name = net.ReadString()		--TODO: Check if name is Russian AND change len limit
	if #first_name < 6 then
		net.Start("urp.chars.char_create_status")
		net.WriteBool(false)
		net.Send(ply)
		return
	end
	local last_name = net.ReadString()
	if #last_name < 6 then
		net.Start("urp.chars.char_create_status")
		net.WriteBool(false)
		net.Send(ply)
		return
	end
	local sex = net.ReadString()  --TODO: Save player sex type in DB
	if not urp.config.chars.models[sex] then
		net.Start("urp.chars.char_create_status")
		net.WriteBool(false)
		net.Send(ply)
		return
	end

	local model = net.ReadString()
	if not urp.config.chars.models[sex][model] then
		net.Start("urp.chars.char_create_status")
		net.WriteBool(false)
		net.Send(ply)
		return
	end

	local model_skin = net.ReadInt(16)
	urp.chars.create_character(ply, first_name, last_name, model, model_skin)
end)

local meta = FindMetaTable("Player")

function meta:SetPlayerModel(model)
	self:SetNWString("urp.PlayerModel", model)
end

function meta:SetFirstName(name)
	self:SetNWString("urp.FirstName", name)
end

function meta:SetLastName(name)
	self:SetNWString("urp.LastName", name)
end
