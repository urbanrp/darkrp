urp.chars = urp.chars or {}

local main
function urp.chars.open_menu()
	local characters = net.ReadTable()
	if IsValid(main) then main:Remove() end

	main = vgui.Create("DFrame")
	main:SetSize(ScrW(), ScrH())
	main:SetPos(0,0)
	main:SetAlpha(0)
	main:AlphaTo(255,1)
	main:MakePopup()
	main:SetTitle('')
	main:ShowCloseButton(false)
	main:SetDraggable(false)
	main.Close = function()
		main:AlphaTo(0,1,0,function()
			hook.Remove("HUDShouldDraw", "urp.chars.hide_hud")
			hook.Remove("CalcView", "urp.chars.CalcView")
			main:Remove()
			gui.EnableScreenClicker( false )
		end)
	end
	main.is_loading = false
	surface.SetFont(urp.util.GetFont(70))
	local x = surface.GetTextSize("Urban")
	local x1 = surface.GetTextSize("RP")

	main.Paint = function(self,w,h)
		urp.util.DrawRect(0,0,w,h,Color(0,0,0,100))
		urp.util.DrawBlur(self,5)

		if self.is_loading then
			urp.util.DrawShadowText("Loading...", urp.util.GetFont(20), ScrW()*.5, ScrH()*.5, Color(255,255,255), 1, 1)
		end

		urp.util.DrawShadowText("Urban", urp.util.GetFont(70), ScrW()*.5 - x*.5 - x1*.5, 0, urp.colors.URP, 0, 0)
		urp.util.DrawShadowText("RP", urp.util.GetFont(70), ScrW()*.5 + x*.5 - x1*.5, 0, Color(255,255,255), 0, 0)
	end

	main.sub = {}
	main.Loading = function(self, b)
		main.is_loading = b
		if not b then
			for k,v in pairs(main.sub) do
				v:SetVisible(true)
			end

			gui.EnableScreenClicker( true )
		else
			for k,v in pairs(main.sub) do
				v:SetVisible(false)
			end

			gui.EnableScreenClicker( false )
		end
	end

	local CreateCharacterMenu
	main.OpenCreateCharacterMenu = function()
		if IsValid(CreateCharacterMenu) then
			return
		end

		for k,v in pairs(main.sub) do
			v:Remove()
		end
		main.sub = {}

		CreateCharacterMenu = vgui.Create("DPanel", main)
		main.sub[#main.sub + 1] = CreateCharacterMenu
		CreateCharacterMenu:SetPos(ScrW() * .1, ScrH() * .1)
		CreateCharacterMenu:SetSize(ScrW() * .8,ScrH() * .8)
		CreateCharacterMenu:SetAlpha(0)
		CreateCharacterMenu:AlphaTo(255,0.4)
		CreateCharacterMenu.Paint = function(self, w, h)
			urp.util.DrawRect(0,0,w,h,Color(0,0,0,100))
		end
		CreateCharacterMenu.Notify = function(self, text, col)
			if not IsValid(CreateCharacterMenu.scroll) then return end

			local scroll = CreateCharacterMenu.scroll
			local notify = vgui.Create("DPanel", scroll)
			notify:Dock(TOP)
			notify:DockMargin(5,2,2,2)
			notify:SetAlpha(0)
			notify:AlphaTo(255,0.4)
			notify.Paint = function(self,w,h)
				urp.util.DrawBox(0,0,w,h, ColorAlpha(col, 50), col)
				urp.util.DrawShadowText(text, urp.util.GetFont(13), w*.5, h*.5, Color(255,255,255), 1, 1)
			end

			timer.Simple(6, function()
				if not IsValid(notify) then return end
				notify:AlphaTo(0,0.4,0,function()
					if not IsValid(notify) then return end
					notify:Remove()
				end)
			end)
		end

		local goback = vgui.Create("DButton", main)
		main.sub[#main.sub + 1] = goback
		goback:SetSize(CreateCharacterMenu:GetWide()*.2, ScrH()*.05)
		goback:SetPos(ScrW() * .1, ScrH() * .9 + 5)
		goback:SetFont(urp.util.GetFont(10))
		goback:SetText("Вернутся")
		goback:SetTextColor(Color(255,255,255))
		goback.Paint = function(self,w,h)
			urp.util.DrawRect(0,0,w,h,Color(0,0,0,100))
		end
		goback.DoClick = function(self)
			surface.PlaySound("garrysmod/ui_click.wav")
			main:OpenMainMenu()
		end
		goback.OnCursorEntered = function(self)
			surface.PlaySound("garrysmod/ui_hover.wav")
		end

		local character = vgui.Create("DModelPanel", CreateCharacterMenu)
		character._Paint = character.Paint
		character:SetSize(CreateCharacterMenu:GetWide()*.25, CreateCharacterMenu:GetTall() - 4)
		character:SetPos(2,2)
		character:SetModel(table.GetRandomKey(urp.config.chars.models.male))
		character:SetFOV(40)
		character.UpdateAnim = function()
			character:SetAnimated( true )
			local anim = character:GetEntity():LookupSequence( "pose_standing_02" )
			character:GetEntity():SetSequence( anim )
		end
		character:UpdateAnim()
		character.Paint = function(self,w,h)
			urp.util.DrawRect(0,0,w,h,Color(0,0,0,100))

			self:_Paint(w,h)
		end
		character.Angles = Angle(0,49,0)
		function character:DragMousePress()
			self.PressX, self.PressY = gui.MousePos()
			self.Pressed = true
		end

		function character:DragMouseRelease()
			self.Pressed = false
		end

		function character:LayoutEntity( ent )
			if ( self.bAnimated ) then self:RunAnimation() end

			if ( self.Pressed ) then
				local mx, my = gui.MousePos()
				self.Angles = self.Angles - Angle( 0, ( self.PressX or mx ) - mx, 0 )

				self.PressX, self.PressY = gui.MousePos()
			end

			ent:SetAngles(  self.Angles )
		end

		character.OnCursorEntered = function(self)
			surface.PlaySound("garrysmod/ui_hover.wav")
		end

		local scroll = vgui.Create("DScrollPanel", CreateCharacterMenu)
		scroll:SetPos(character:GetWide() + 4, 2)
		CreateCharacterMenu.scroll = scroll
		scroll:SetSize(CreateCharacterMenu:GetWide() - character:GetWide() - 6, CreateCharacterMenu:GetTall() - 4)
		scroll.Paint = function(self,w,h)
			urp.util.DrawRect(0,0,w,h,Color(0,0,0,100))
		end

		scroll.VBar:SetWide(3)

		local bar = scroll.VBar
		bar.Paint = function(self, w, h)
		end
		bar.btnUp.Paint = function(self, w, h)
		end
		bar.btnDown.Paint = function(self, w, h)
		end


		local name_p = vgui.Create("DPanel", scroll)
		name_p:Dock(TOP)
		name_p:DockMargin(2,1,2,1)
		name_p:SetTall(scroll:GetTall()*.06)
		name_p.Paint = function(self,w,h)
			urp.util.DrawRect(0,0,w,h,Color(0,0,0,100))
			urp.util.DrawShadowText("Имя", urp.util.GetFont(10), 5, 0, Color(255,255,255), 0, 0)
		end

		local nemt_te = vgui.Create("DTextEntry", name_p)
		nemt_te:SetPos(2, name_p:GetTall() - 27)
		nemt_te:SetSize(scroll:GetWide() - 6, 25)
		nemt_te:SetText( "" )

		local subname_p = vgui.Create("DPanel", scroll)
		subname_p:Dock(TOP)
		subname_p:DockMargin(2,1,2,1)
		subname_p:SetTall(scroll:GetTall()*.06)
		subname_p.Paint = function(self,w,h)
			urp.util.DrawRect(0,0,w,h,Color(0,0,0,100))
			urp.util.DrawShadowText("Фамилия", urp.util.GetFont(10), 5, 0, Color(255,255,255), 0, 0)
		end

		local subnemt_te = vgui.Create("DTextEntry", subname_p)
		subnemt_te:SetPos(2,subname_p:GetTall() - 27)
		subnemt_te:SetSize(scroll:GetWide() - 6, 25)
		subnemt_te:SetText( "" )

		local picked_gender = 'male'
		local gender_p = vgui.Create("DPanel", scroll)
		gender_p:Dock(TOP)
		gender_p:DockMargin(2,1,2,1)
		gender_p:SetTall(scroll:GetTall()*.06)
		gender_p.Paint = function(self,w,h)
			urp.util.DrawRect(0,0,w,h,Color(0,0,0,100))
			urp.util.DrawShadowText("Пол", urp.util.GetFont(10), 5, 0, Color(255,255,255), 0, 0)
		end

		local gender_cb = vgui.Create("DComboBox", gender_p)
		gender_cb:SetPos(2, gender_p:GetTall() - 27)
		gender_cb:SetSize(scroll:GetWide() - 6, 25)
		gender_cb:AddChoice( "Женский", false )
		gender_cb:AddChoice( "Мужской", true, true )

		local models = {}
		local model_p = vgui.Create("DPanel", scroll)
		model_p:Dock(TOP)
		model_p:DockMargin(2,1,2,1)
		model_p:SetTall(scroll:GetTall()*.07)
		model_p.CreateModels = function(self, m)
			character:SetModel(table.GetRandomKey(urp.config.chars.models[m]))
			character:UpdateAnim()
			for k,v in pairs(urp.config.chars.models[m]) do
				local model = vgui.Create("urp_modelicon", model_p)
				models[#models + 1] = model
				model:Dock(LEFT)
				model:DockMargin(2,1,2,1)
				model:SetModel(k)
				model.model = k
				model.DoClick = function(self)
					character:SetModel(self.model)
					character:UpdateAnim()
				end
			end
		end
		model_p.Paint = function(self,w,h)
			urp.util.DrawRect(0,0,w,h,Color(0,0,0,100))
		end

		function gender_cb:OnSelect( index, text, data )
			for k,v in pairs(models) do
				v:Remove()
			end

			local m = data and "male" or "female"
			picked_gender = m
			model_p:CreateModels(m)
		end
		model_p:CreateModels('male')

		local skin_p = vgui.Create("DPanel", scroll)
		skin_p:Dock(TOP)
		skin_p:DockMargin(2,1,2,1)
		skin_p:SetTall(scroll:GetTall()*.07)
		skin_p.Paint = function(self,w,h)
			urp.util.DrawRect(0,0,w,h,Color(0,0,0,100))
			urp.util.DrawShadowText("Скин", urp.util.GetFont(10), w*.5, 0, Color(255,255,255), 1, 0)
		end

		local skin_sl = vgui.Create("urp_slider", skin_p)
		skin_sl:SetPos(2, skin_p:GetTall() - 27)
		skin_sl:SetSize(scroll:GetWide() - 6, 25)

		local create = vgui.Create("DButton", main)
		main.sub[#main.sub + 1] = create
		create:SetSize(CreateCharacterMenu:GetWide()*.2, ScrH()*.05)
		create:SetPos(ScrW() * .9 - create:GetWide(), ScrH() * .9 + 5)
		create:SetFont(urp.util.GetFont(10))
		create:SetText("Создать")
		create:SetTextColor(Color(255,255,255))
		create.Paint = function(self,w,h)
			urp.util.DrawRect(0,0,w,h,Color(0,0,0,100))
		end
		create.DoClick = function(self)
			surface.PlaySound("garrysmod/ui_click.wav")

			local name = nemt_te:GetText()
			if name == nil or #name < 6 then return end
			local subname = subnemt_te:GetText()
			if subname == nil or #subname < 6 then return end
			net.Start("urp.chars.create_char")
				net.WriteString(name)
				net.WriteString(subname)
				net.WriteString(picked_gender)
				net.WriteString(character:GetEntity():GetModel())
			net.SendToServer()
			main:Loading(true)
		end
		create.OnCursorEntered = function(self)
			surface.PlaySound("garrysmod/ui_hover.wav")
		end
	end


	local anims = {
		"pose_standing_01",
		"pose_standing_02",
		-- "pose_standing_03",
		"pose_standing_04",
		-- "pose_ducking_01"
	}

	main.OpenMainMenu = function()
		for k,v in pairs(main.sub) do
			v:Remove()
		end
		main.sub = {}

		local character, pick_char
		local function OpenCharacter(id, mdl, status)
			if not IsValid(character) then
				character = vgui.Create("DModelPanel", main)
				character._Paint = character.Paint
				main.sub[#main.sub + 1] = character

				character:SetSize(ScrW()*.25, ScrH()*.7)
				character:Center()
			end
			character:SetModel(mdl)
			character:SetAnimated( true )
			local anim = character:GetEntity():LookupSequence( anims[math.random(#anims)] )
			character:GetEntity():SetSequence( anim )
			character:SetFOV(40)
			character.status = status or 1
			character.id = id
			character.Paint = function(self,w,h)
				if self.status == 1 then
					urp.util.DrawRect(0,0,w,h,Color(0,0,0,100))
				end

				self:_Paint(w,h)

				if self.status == 2 then
					local cin = (math.sin(CurTime() * 6) + 1) * .5
					urp.util.DrawRect(0,0,w,h,Color(cin * 255, 0, 255 - (cin * 255), 100))
					urp.util.DrawRect(0,h*.25,w,h*.15,Color(0,0,0,100))
					urp.util.DrawShadowText("Арестован!", urp.util.GetFont(15), w*.5, h*.325, Color(255,255,255), 1, 1)
				elseif self.status == 3 then
					urp.util.DrawRect(0,0,w,h,Color(200,0,0,100))
					urp.util.DrawRect(0,h*.25,w,h*.15,Color(0,0,0,100))
					urp.util.DrawShadowText("Убит!", urp.util.GetFont(15), w*.5, h*.325, Color(255,255,255), 1, 1)
				end
			end
			character.Angles = Angle(0,49,0)
			function character:DragMousePress()
				self.PressX, self.PressY = gui.MousePos()
				self.Pressed = true
			end

			function character:DragMouseRelease()
				self.Pressed = false
			end

			function character:LayoutEntity( ent )
				if ( self.bAnimated ) then self:RunAnimation() end

				if ( self.Pressed ) then
					local mx, my = gui.MousePos()
					self.Angles = self.Angles - Angle( 0, ( self.PressX or mx ) - mx, 0 )

					self.PressX, self.PressY = gui.MousePos()
				end

				ent:SetAngles(  self.Angles )
			end

			if not IsValid(pick_char) then
				pick_char = vgui.Create("DButton", character)
				pick_char:SetSize(character:GetWide() - 4, ScrH()*.05)
				pick_char:SetPos(2,character:GetTall() - pick_char:GetTall() - 2)
				pick_char:SetText('')
			end
			pick_char.Paint = function(self,w,h)
				if self.status == 2 then
					local cin = (math.sin(CurTime() * 6) + 1) * .5
					urp.util.DrawRect(0,0,w,h,Color(cin * 255, 0, 255 - (cin * 255), 100))
					urp.util.DrawShadowText("Арестован!", urp.util.GetFont(15), w*.5, h*.5, Color(255,255,255), 1, 1)
				elseif self.status == 3 then
					urp.util.DrawRect(0,0,w,h,Color(200,0,0,100))
					urp.util.DrawRect(0,h*.25,w,h*.15,Color(0,0,0,100))
					urp.util.DrawShadowText("Убит!", urp.util.GetFont(15), w*.5, h*.5, Color(255,255,255), 1, 1)
				else
					urp.util.DrawRect(0,0,w,h,Color(0,0,0,100))
					urp.util.DrawShadowText("Выбрать персонажа", urp.util.GetFont(15), w*.5, h*.5, Color(255,255,255), 1, 1)
				end
			end

			pick_char.DoClick = function(self)
				surface.PlaySound("garrysmod/ui_click.wav")
				main:Loading(true)
				net.Start("urp.chars.load_character")
					net.WriteInt(character.id, 32)
				net.SendToServer()
			end

			pick_char.OnCursorEntered = function(self)
				surface.PlaySound("garrysmod/ui_hover.wav")
			end

			net.Receive("urp.chars.load_character_status", function()
				local b = net.ReadBool()
				if b then
					main:Close()
				else
					main:Loading(false)
				end
			end)
		end

		local characters_list = vgui.Create("DScrollPanel", main)
		main.sub[#main.sub + 1] = characters_list
		characters_list:SetPos(0,ScrH()*.15)
		characters_list:SetSize(ScrW()*.2, ScrH()*.7)
		characters_list.Paint = function(self,w,h)
			urp.util.DrawRect(0,0,w,h,Color(0,0,0,100))
		end

		characters_list.VBar:SetWide(3)

		local bar = characters_list.VBar
		bar.Paint = nil
		bar.btnUp.Paint = nil
		bar.btnDown.Paint = nil

		local created_chars = {}
		main.UpdateChars = function()
			if not IsValid(characters_list) then return end
			for k,v in ipairs(created_chars) do
				v:Remove()
			end
			created_chars = {}

			for k,v in pairs(characters) do
				local char = vgui.Create("DButton", characters_list)
				created_chars[#created_chars + 1] = char
				char:SetText('')
				char:Dock(TOP)
				char:DockMargin(2,1,2,1)
				char:SetTall(characters_list:GetTall() *.15)
				char.Paint = function(self,w,h)
					urp.util.DrawRect(0,0,w,h,Color(0,0,0,100))
					local y = select(2, urp.util.DrawShadowText(v.first_name, urp.util.GetFont(15), h, 0, Color(255,255,255), 0, 0))
					urp.util.DrawShadowText(v.last_name, urp.util.GetFont(15), h, y, Color(255,255,255), 0, 0)
					urp.util.DrawShadowText(urp.format_money(v.money_held), urp.util.GetFont(15), h, y*2, Color(255,255,255), 0, 0)
				end
				char.DoClick = function(self)
					surface.PlaySound("garrysmod/ui_click.wav")
					OpenCharacter(v.id, v.model)
				end
				char.OnCursorEntered = function(self)
					surface.PlaySound("garrysmod/ui_hover.wav")
				end

				local model = vgui.Create("DModelPanel", char)
				model:SetPos(0,0)
				model:SetSize(char:GetTall(), char:GetTall())
				model:SetModel(v.model)
				model:SetFOV(45)
				model:SetCamPos(Vector(20, -10, 64))
				model:SetLookAt(Vector(0, 0, 64))
				function model:LayoutEntity()
					return
				end
			end
		end

		main:UpdateChars()

		local create_new_char = vgui.Create("DButton", main)
		main.sub[#main.sub + 1] = create_new_char
		create_new_char:SetSize(characters_list:GetWide()*.8, ScrH()*.05)
		create_new_char:SetPos(characters_list:GetWide()*.1, ScrH()*.85 + 5)
		create_new_char:SetFont(urp.util.GetFont(10))
		create_new_char:SetText("Создать персонажа")
		create_new_char:SetTextColor(Color(255,255,255))
		create_new_char.Paint = function(self,w,h)
			urp.util.DrawRect(0,0,w,h,Color(0,0,0,100))
		end
		create_new_char.DoClick = function(self)
			surface.PlaySound("garrysmod/ui_click.wav")
			main:OpenCreateCharacterMenu()
		end
		create_new_char.OnCursorEntered = function(self)
			surface.PlaySound("garrysmod/ui_hover.wav")
		end
	end

	main:OpenMainMenu()



	net.Receive("urp.chars.name_is_taken", function()
		main:Loading(false)

		if not IsValid(CreateCharacterMenu) then return end

		CreateCharacterMenu:Notify("Такое сочетание имени и фамилии занято!", Color(255,0,0))
	end)

	net.Receive("urp.chars.char_create_status", function()
		local b = net.ReadBool() or false
		main:Loading(false)

		if b then
			main:OpenMainMenu()
			local data = net.ReadTable()
			data.money_held = urp.config.default_money
			characters[#characters + 1] = data
			main:UpdateChars()
		end
	end)

	hook.Add( "HUDShouldDraw", "urp.chars.hide_hud", function()	return false end )

	local campos = urp.config.chars.cam_positions[1].pos
	local camang = urp.config.chars.cam_positions[1].ang
	local id = 2
	local sec_campos = urp.config.chars.cam_positions[id].pos
	local sec_camang = urp.config.chars.cam_positions[id].ang

	hook.Add( "CalcView", "urp.chars.CalcView", function(ply, pos, angles, fov)  -- TODO: NEED MORE SMOOTH
		if campos:Distance(sec_campos) < 25 then
			id = id + 1
			if id > #urp.config.chars.cam_positions then id = 1 end
			sec_campos = urp.config.chars.cam_positions[id].pos
			sec_camang = urp.config.chars.cam_positions[id].ang
		end

		local view = {}

		campos = LerpVector( 0.002, campos, sec_campos )
		camang = LerpAngle( 0.002, camang, sec_camang )
		view.origin = campos
		view.angles = camang
		view.fov = fov
		view.drawviewer = true

		return view
	end)
end
net.Receive("urp.chars.open_menu", urp.chars.open_menu)
