util.AddNetworkString("urp.notify")

function urp.notify(ply, type, len, text)
	net.Start("urp.notify")
	net.WriteString(text)
	net.WriteUInt(type, 8)
	net.WriteUInt(len, 8)
	net.Send(ply)
end

function urp.notify_all(ply, type, len, text)
	net.Start("urp.notify")
	net.WriteString(text)
	net.WriteUInt(type, 8)
	net.WriteUInt(len, 8)
	net.Broadcast()
end
