local PANEL = {}

function PANEL:Init()
	if IsValid(self.Knob) then
		self.Knob.Paint = function( panel, w, h )
			if panel:IsHovered() then
				surface.SetDrawColor(ColorAlpha(urp.colors.URP, 200))
				draw.NoTexture()
				urp.util.DrawCircle(w*.5, h*.5, h*.5, 60)
			else
				surface.SetDrawColor(ColorAlpha(urp.colors.URP, 150))
				draw.NoTexture()
				urp.util.DrawCircle(w*.5, h*.5, h*.5, 60)
			end
		end
	end
end

function PANEL:Paint(w,h)
	urp.util.DrawRect(0,0,w,h,urp.colors.Background)
end

derma.DefineControl( "urp_slider", "", PANEL, "DSlider" )
