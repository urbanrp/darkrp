local PANEL = {}

function PANEL:Init()
	self:SetModel(LocalPlayer():GetModel())
end

function PANEL:Paint(w, h)
	urp.util.DrawBox(0, 0, w, h, ColorAlpha(urp.colors.URP, 100))
	if self:IsHovered() then
		urp.util.DrawBox(1, 1, w - 2, h - 2, ColorAlpha(urp.colors.URP, 150))
	end
end

function PANEL:PaintOver(w, h)
end

vgui.Register('urp_modelicon', PANEL, 'SpawnIcon')
