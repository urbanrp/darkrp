local meta = FindMetaTable("Player")

function meta:IsDev()
	return self:SteamID() == "STEAM_0:1:35205533"
end
