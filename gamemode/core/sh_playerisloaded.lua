local tag = "PlayerIsLoaded"

if SERVER then
	util.AddNetworkString(tag)

	net.Receive(tag, function(_, ply)
		if ply.player_is_loaded then return end

		hook.Run(tag, ply)

		ply.player_is_loaded = true
	end)
else
	hook.Add("Think", tag, function()
		if not IsValid(LocalPlayer()) then return end

		net.Start(tag)
		net.SendToServer()

		hook.Run(tag, LocalPlayer())
		hook.Remove("Think", tag)
	end)
end
