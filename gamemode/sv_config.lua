urp.mysql_config = {
	Enabled = false,
	Host = "localhost",
	Username = "root",
	Password = "",
	Database_name = "urbanrp",
	Database_port = 3306,
	Preferred_module = "mysqloo" -- must be either "mysqloo" or "tmysql4"
}

MySQLite.initialize(urp.mysql_config)
